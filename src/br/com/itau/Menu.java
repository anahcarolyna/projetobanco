package br.com.itau;

import javax.swing.*;

public class Menu {
      public static void inicializarAplicacao() {
          Cliente cliente =  new Cliente();
          cliente.incluirClienteTeste();

          Object[] options = new Object[]{"Novo cliente", "Login"};
          String menu = "";

          int selecao = JOptionPane.showOptionDialog(null, "Favor selecione a opção desejada:",
                  "Seja bem vindo ao Banco Itau", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
          if (selecao == 0) {
               Operador.efetuarCadastro(cliente);
          }

          selecao = JOptionPane.showConfirmDialog(null, "Deseja efetuar o login?",
                  "Seja bem vindo ao Banco Itau", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

          if (selecao == 0) {
              cliente = Cliente.efetuarLogin(JOptionPane.showInputDialog("Nome:"), JOptionPane.showInputDialog("Senha:"));

              if (cliente != null) {
                  do {
                      menu = JOptionPane.showInputDialog(null, "Seja bem vinda: " +
                              cliente.getNome() + "\nSeleciona a opção desejada:" +
                              "\n1- Consultar saldo" +
                              "\n2- Depositar" +
                              "\n3- Sacar" +
                              "\n4- Sair" +
                              "\nEscolha uma opção.","Banco Itau", JOptionPane.INFORMATION_MESSAGE);

                      switch (menu) {
                          case "1":
                              JOptionPane.showMessageDialog(null, "Saldo atual: R$" + cliente.getSaldoConta());
                              break;
                          case "2":
                              Operador.depositar(cliente.getNome(), Double.parseDouble(JOptionPane.showInputDialog("Valor:")));
                              JOptionPane.showMessageDialog(null, "Saldo atual: R$" + cliente.getSaldoConta());
                              break;
                          case "3":
                              double valorSaque = Double.parseDouble(JOptionPane.showInputDialog("Valor:"));
                              if (cliente.getSaldoConta() >= valorSaque) {
                                  Operador.sacar(cliente.getNome(), valorSaque);
                                  JOptionPane.showMessageDialog(null, "Saldo atual: R$" + cliente.getSaldoConta());
                              } else {
                                  JOptionPane.showMessageDialog(null, "Saldo insuficiente");
                              }
                              break;
                          case "4":
                              int valorSelecionadaBotao = JOptionPane.showConfirmDialog(null, "Deseja sair do sistema?", "Sair", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                              if (valorSelecionadaBotao == 0) {
                                  System.exit(0);
                              }
                          default:
                              JOptionPane.showMessageDialog(null, "Opção inválida! Escolha uma opção válida");

                      }
                  } while (Integer.parseInt(menu) >= 1 && Integer.parseInt(menu) <= 4);
              }
              else {
                  JOptionPane.showMessageDialog(null, "Cliente não cadastrado!");
              }
          }
          System.exit(0);
      }
}

package br.com.itau;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Cliente {
    private String nome;
    private String dataNasc;
    private int agencia = 1000;
    private int conta = 51000;
    private  int dac = 0;
    private  String senha;
    private double saldoConta;
    private static ArrayList<Cliente> cliente;

    public Cliente(String nome, String dataNascimento, String senha, double saldo){
        //cliente = new ArrayList();
        this.nome = nome;
        this.dataNasc = dataNascimento;
        this.senha = senha;
        this.saldoConta = saldo;
    }
    public Cliente(){
        cliente = new ArrayList();
    }


    public int getAgencia() {
        return agencia;
    }
    public int getConta() {
        return conta++;
    }
    public int getDac() {
        return dac++;
    }

    public String getNome() {
        return nome;
    }

    public void setSaldoConta(double saldoConta) {
        this.saldoConta += saldoConta;
    }

    public double getSaldoConta() {
        return saldoConta;
    }

    public String getSenha() {
        return senha;
    }

    public void incluirCliente(Cliente novoCliente){
        novoCliente.agencia = getAgencia();
        novoCliente.conta = getConta();
        novoCliente.dac = getDac();
        cliente.add(novoCliente);
    }

    public void incluirClienteTeste(){
        for (int contador = 0; contador <= 15; contador++) {
            Cliente cliente = new Cliente("Fulano" + contador,
                    (contador + 1) + "/" + 01 + "/1986",
                    contador + "teste", contador * contador);

            this.incluirCliente(cliente);
        }
    }

    public static Cliente listarCliente(String nome){
        for (Cliente clienteAlterar : cliente) {
            if (clienteAlterar.getNome().equalsIgnoreCase(nome)) {
                return clienteAlterar;
            }
        }
        return null;
    }
    public static Cliente efetuarLogin(String nome, String senha){

        for (Cliente cliente : cliente) {
            if (cliente.getNome().equalsIgnoreCase(nome) && cliente.getSenha().equals(senha)) {
                return cliente;
            }
        }
        return null;
    }
    public String buscarTodos()
    {
        String imprimirContato = "";
        for(Cliente cliente : this.cliente) {
            if (cliente.nome.equalsIgnoreCase(this.nome)) {
                imprimirContato += "Agencia:" + cliente.agencia + "\nConta:" + cliente.conta + "-" +
                        cliente.dac + "\nSaldo:" + cliente.saldoConta;
            }
        }
        return imprimirContato;
    }

    public static int CalculaIdade(Date dataNasc){
        Calendar nascimento = new GregorianCalendar();
        nascimento.setTime(dataNasc);

        Calendar calendario = Calendar.getInstance();

        int Idade = calendario.get(Calendar.YEAR) - nascimento.get(Calendar.YEAR);

        nascimento.add(Calendar.YEAR, Idade);

        if (calendario.before(nascimento)) {
            Idade--;
        }
        return Idade;
    }
}

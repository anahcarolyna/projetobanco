package br.com.itau;

import javax.swing.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Operador {
    public static void depositar(String nome, double valorDepositar){
        Cliente clienteCadastrado = Cliente.listarCliente(nome);
        clienteCadastrado.setSaldoConta(valorDepositar) ;
    }

    public static void sacar(String nome, double valorSaque){
        Cliente clienteCadastrado = Cliente.listarCliente(nome);
        clienteCadastrado.setSaldoConta(-valorSaque);
    }

    public static void efetuarCadastro(Cliente novoCliente) {
        DateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        formato.setLenient(false);
        Date nasc = new Date();

        try {
            nasc = formato.parse(JOptionPane.showInputDialog(null, "Por favor, informe sua data de nascimento: ", "Dados iniciais", JOptionPane.OK_OPTION));
            if (Cliente.CalculaIdade(nasc) < 18) {
                JOptionPane.showMessageDialog(null, "Cadastro autorizado somente para pessoas a partir dos 18 anos");
            } else {
                novoCliente = new Cliente(JOptionPane.showInputDialog("Nome:"),
                        nasc.toString(),
                        JOptionPane.showInputDialog("Senha"), 0);

                novoCliente.incluirCliente(novoCliente);
                JOptionPane.showMessageDialog(null, "Cliente incluído com sucesso!");
                JOptionPane.showMessageDialog(null, novoCliente.buscarTodos());
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Data inválida");
        }
    }
}
